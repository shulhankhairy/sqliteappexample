package com.hexavara.khairy.sqliteapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    DatabaseHandler db;
    Button insertBtn;
    EditText nimEt,namaEt,ttlEt,alamatEt,noHpEt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        insertBtn = findViewById(R.id.btn_input);
        nimEt = findViewById(R.id.et_nim);
        namaEt = findViewById(R.id.et_nama);
        ttlEt = findViewById(R.id.et_ttl);
        alamatEt = findViewById(R.id.et_alamat);
        noHpEt = findViewById(R.id.et_hp);

        db = new DatabaseHandler(getApplicationContext());

        insertBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int nimStr = Integer.parseInt(nimEt.getText().toString());
                String namaStr = namaEt.getText().toString();
                String ttlStr = ttlEt.getText().toString();
                String alamatStr = alamatEt.getText().toString();
                String noHpStr = noHpEt.getText().toString();

                db.addRecord(new MhsModel(nimStr,namaStr,ttlStr,alamatStr,noHpStr));

                Intent intent = new Intent(getApplicationContext(),ViewDataActivity.class);
                startActivity(intent);
            }
        });
    }
}
