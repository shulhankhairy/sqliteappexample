package com.hexavara.khairy.sqliteapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHandler extends SQLiteOpenHelper {

    //versi database yang digunakan
    private static final int DB_VERSION = 1;

    //nama database yang dibuat
    private static final String DB_NAME = "polinema";

    //nama tabel yang dibuat
    private static final String TABLE_NAME = "mahasiswa";

    //nama kolom yang dibuat
    private static final String KEY_NIM = "nim";
    private static final String KEY_NAMA = "nama";
    private static final String KEY_TTL = "ttl";
    private static final String KEY_ALAMAT = "alamat";
    private static final String KEY_NOHP = "noHp";

    public DatabaseHandler(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME
                + "(" + KEY_NIM + " INTEGER PRIMARY KEY,"
                + KEY_NAMA + " TEXT,"
                + KEY_TTL + " TEXT,"
                + KEY_ALAMAT + " TEXT,"
                + KEY_NOHP + " TEXT" + ")";

        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
        db.execSQL(DROP_TABLE);
    }

    //insert data
    public void addRecord(MhsModel mhsModel){
        SQLiteDatabase db  = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NIM, mhsModel.getNim());
        values.put(KEY_NAMA, mhsModel.getNama());
        values.put(KEY_TTL, mhsModel.getTtl());
        values.put(KEY_ALAMAT, mhsModel.getAlamat());
        values.put(KEY_NOHP, mhsModel.getNoHp());

        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    //mengambil baris data
    public MhsModel getMhsData(int nim){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_NAME,
                new String[] {KEY_NIM, KEY_NAMA, KEY_TTL, KEY_ALAMAT, KEY_NOHP},
                KEY_NIM + "=?",
                new String[]{String.valueOf(nim)},
                null,
                null,
                null,
                null);

        if (cursor != null) cursor.moveToFirst();

        //objek mahasiswa
        MhsModel mahasiswaw = new MhsModel(cursor.getInt(0),        //index kolom nim
                                            cursor.getString(1),    //index kolom nama
                                            cursor.getString(2),    //index kolom ttl
                                            cursor.getString(3),    //index kolom alamat
                                            cursor.getString(4));   //index kolom ho hp

        //return data mahasiswa
        return mahasiswaw;
    }

    //mengambil semua data
    public List<MhsModel> getAllMhsData(){
        List<MhsModel> listMhs = new ArrayList<MhsModel>();

        //query untuk ambil semua data
        String getAllDataQuery = "SELECT * FROM " + TABLE_NAME;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(getAllDataQuery,null);

        //
        if (cursor.moveToFirst()){
            do {
                MhsModel mhs = new MhsModel();
                mhs.setNim(cursor.getInt(0));
                mhs.setNama(cursor.getString(1));
                mhs.setTtl(cursor.getString(2));
                mhs.setAlamat(cursor.getString(3));
                mhs.setNoHp(cursor.getString(4));
            } while (cursor.moveToNext());
        }

        return listMhs;
    }

    //update data pada tabel
    public int updateMhs(MhsModel mhs){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put(KEY_NAMA, mhs.getNama());
        cv.put(KEY_TTL, mhs.getTtl());
        cv.put(KEY_ALAMAT, mhs.getAlamat());
        cv.put(KEY_NOHP, mhs.getNoHp());

        //update row
        return db.update(TABLE_NAME, cv, KEY_NIM + "=?",
                new String[]{String.valueOf(mhs.getNim())});
    }

    //hapus data pada tabel
    public void deleteMhs(MhsModel mhs) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, KEY_NIM + " = ?",
                new String[] { String.valueOf(mhs.getNim()) });
        db.close();
    }
}
