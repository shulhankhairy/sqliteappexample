package com.hexavara.khairy.sqliteapp;

public class MhsModel {
    int nim;
    String nama,ttl,alamat,noHp;

    public MhsModel() {

    }

    public MhsModel(int nim, String nama, String ttl, String alamat, String noHp) {
        this.nim = nim;
        this.nama = nama;
        this.ttl = ttl;
        this.alamat = alamat;
        this.noHp = noHp;
    }

    public int getNim() {
        return nim;
    }

    public void setNim(int nim) {
        this.nim = nim;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTtl() {
        return ttl;
    }

    public void setTtl(String ttl) {
        this.ttl = ttl;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNoHp() {
        return noHp;
    }

    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }
}
