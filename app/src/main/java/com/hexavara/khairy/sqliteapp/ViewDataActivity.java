package com.hexavara.khairy.sqliteapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewDataActivity extends AppCompatActivity {

    @BindView(R.id.tv_view_nama_data)
    TextView tvViewNamaData;
    @BindView(R.id.tv_view_ttl_data)
    TextView tvViewTtlData;
    @BindView(R.id.tv_view_alamat_data)
    TextView tvViewAlamatData;
    @BindView(R.id.tv_view_nohp_data)
    TextView tvViewNohpData;
    @BindView(R.id.tv_view_nim_data)
    TextView tvViewNimData;

    DatabaseHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_data);
        ButterKnife.bind(this);

        db = new DatabaseHandler(getApplicationContext());

        Intent intent = getIntent();
        MhsModel mhsModel = new MhsModel();
        db.getAllMhsData();

        tvViewNimData.setText(mhsModel.getNim());
        tvViewNamaData.setText(mhsModel.getNama());
        tvViewAlamatData.setText(mhsModel.getAlamat());
        tvViewTtlData.setText(mhsModel.getTtl());
        tvViewNohpData.setText(mhsModel.getNoHp());

    }
}
